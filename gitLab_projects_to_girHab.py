import os
import requests

# Set your GitLab and GitHub details
gitlab_url = "https://gitlab.com/api/v4"
gitlab_token = "******************************"
github_username = "************************"
github_token = "*****************************"

gitlab_projects_url = f"{gitlab_url}/projects?owned=true&per_page=100"
gitlab_headers = {"PRIVATE-TOKEN": gitlab_token}
gitlab_projects_response = requests.get(gitlab_projects_url, headers=gitlab_headers)

if gitlab_projects_response.status_code != 200:
    print(f"Failed to get GitLab projects. Status code: {gitlab_projects_response.status_code}")
    exit()

gitlab_projects = gitlab_projects_response.json()

# Check if there are projects to migrate
if not gitlab_projects:
    print("No GitLab projects found to migrate.")
    exit()

# Iterate through GitLab projects and migrate to GitHub
for project in gitlab_projects:
    gitlab_repo_url = project["http_url_to_repo"]
    gitlab_repo_name = project["name"]
    project_visibility = project.get("visibility", "private")  # Default to private if visibility is not available

    # Replace spaces with underscores in the repository name
    github_repo_name_no_spaces = gitlab_repo_name.replace(' ', '_')

    # Create a new repository on GitHub
    github_create_repo_url = f"https://api.github.com/user/repos"
    github_create_repo_data = {
        "name": github_repo_name_no_spaces,
        "private": project_visibility == "private"
    }
    github_create_repo_headers = {
        "Authorization": f"token {github_token}"
    }

    response = requests.post(
        github_create_repo_url,
        json=github_create_repo_data,
        headers=github_create_repo_headers
    )

    if response.status_code != 201:
        print(f"Failed to create GitHub repository for {gitlab_repo_name}. Status code: {response.status_code}")
        continue

    print(f"GitHub repository '{github_repo_name_no_spaces}' created successfully.")

    # Clone, add remote, and push to GitHub
    clone_command = f"git clone --mirror {gitlab_repo_url} temp_repo"
    os.system(clone_command)

    os.chdir("temp_repo")
    os.system(f"git remote add github https://github.com/{github_username}/{github_repo_name_no_spaces}.git")
    os.system("git push --mirror github")
    os.chdir("..")
    os.system("rm -rf temp_repo")

print("All repository migrations completed successfully.")
